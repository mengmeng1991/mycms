<?php


namespace Modules\System\Http\ViewComposer;


use Illuminate\View\View;
use Modules\System\Models\Menu;

class AdminComposer
{
    public function compose(View $view)
    {
        $route = request()->route();
        if ($route && in_array("admin.auth", $route->middleware())) {

            $roles = array_merge(config('role'), (config('role_ext') ?? []));;
            $reqPath = request()->path();
            $prevReqPath = mb_substr($reqPath, 0, strripos($reqPath, "/"));

            $view->with([
                'system_menus' => Menu::toTree(),
                'system_version' => config('app.version'),
                'system_editor' => system_config('default_editor'),
                'current_page_name' => $roles[$reqPath] ?? '未命名页面',
                'prev_page_name' => $roles[$prevReqPath] ?? '',
                'prev_page_url' => strstr($prevReqPath, "/") !== false ? "/" . $prevReqPath : '',
                'auth_admin_user' => auth()->guard('admin')->user()
            ]);
        }
    }
}
