<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => 'admin.auth',
    'namespace' => '\Modules\Mp\Http\Controllers\Admin'
], function () {
    Route::group(['prefix' => 'mp/admin'], function () {



        Route::get('/mp_account', 'MpAccountController@index')->name('mp.mp_account');
        Route::get('/mp_account/create', 'MpAccountController@create')->name('mp.mp_account.create');
        Route::post('/mp_account/create', 'MpAccountController@store');
        Route::get('/mp_account/edit', 'MpAccountController@edit')->name('mp.mp_account.edit');
        Route::post('/mp_account/edit', 'MpAccountController@update');
        Route::post('/mp_account/destroy', 'MpAccountController@destroy');


        Route::get('/mp_article', 'MpArticleController@index')->name('mp.mp_article');
        Route::get('/mp_article/create', 'MpArticleController@create')->name('mp.mp_article.create');
        Route::post('/mp_article/create', 'MpArticleController@store');
        Route::get('/mp_article/edit', 'MpArticleController@edit')->name('mp.mp_article.edit');
        Route::post('/mp_article/edit', 'MpArticleController@update');
        Route::post('/mp_article/destroy', 'MpArticleController@destroy');
        Route::get('/mp_article/preview', 'MpArticleController@preview')->name('mp.mp_article.preview');


        Route::get('/mp_template', 'MpTemplateController@index')->name('mp.mp_template');
        Route::get('/mp_template/create', 'MpTemplateController@create')->name('mp.mp_template.create');
        Route::post('/mp_template/create', 'MpTemplateController@store');
        Route::get('/mp_template/edit', 'MpTemplateController@edit')->name('mp.mp_template.edit');
        Route::post('/mp_template/edit', 'MpTemplateController@update');
        Route::post('/mp_template/destroy', 'MpTemplateController@destroy');
        Route::get('/mp_template/preview', 'MpTemplateController@preview')->name('mp.mp_template.preview');
        Route::post('/mp_template/make', 'MpTemplateController@make')->name('mp.mp_template.make');

        Route::get('/mp_push_log', 'MpPushLogController@index')->name('mp.mp_push_log');
        Route::get('/mp_push_log/create', 'MpPushLogController@create')->name('mp.mp_push_log.create');
        Route::post('/mp_push_log/create', 'MpPushLogController@store');
        Route::get('/mp_push_log/edit', 'MpPushLogController@edit')->name('mp.mp_push_log.edit');
        Route::post('/mp_push_log/edit', 'MpPushLogController@update');
        Route::post('/mp_push_log/destroy', 'MpPushLogController@destroy');
        Route::get('/mp_push_log/preview', 'MpPushLogController@preview')->name('mp.mp_push_log.preview');
        Route::post('/mp_push_log/preview', 'MpPushLogController@sendPreview');
        Route::get('/mp_push_log/push', 'MpPushLogController@pushShow')->name('mp.mp_push_log.push');
        Route::post('/mp_push_log/push', 'MpPushLogController@push');


        /* -curd- */
    });
});
