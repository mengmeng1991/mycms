<?php

namespace Modules\Mp\Http\Controllers\Admin;

use App\Http\Controllers\MyAdminController;

class MpArticleController extends MyAdminController
{
    public $view = 'admin.mp_article.';

    public $model = 'Modules\Mp\Models\MpArticleModel';

    public $request = 'Modules\Mp\Http\Requests\MpArticleRequest';

    public function preview()
    {
        $data = $this->getModel()::with($this->editWith)
            ->find($this->request('id', 'intval'));

        return $this->view($this->view . 'preview', compact('data'));
    }
}
