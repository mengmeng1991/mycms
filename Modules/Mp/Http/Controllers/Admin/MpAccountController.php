<?php

namespace Modules\Mp\Http\Controllers\Admin;

use App\Http\Controllers\MyAdminController;

class MpAccountController extends MyAdminController
{
    public $view = 'admin.mp_account.';

    public $model = 'Modules\Mp\Models\MpAccountModel';

    public $request = 'Modules\Mp\Http\Requests\MpAccountRequest';
}
