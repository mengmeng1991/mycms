let myAdmin = {
    uploadObjArray: [],
    listen: function () {
        this.form();
        this.editor();
        this.upload();
    },
    form: function (selector, callback) {

        selector = selector ? selector : '#app-form';

        if (callback === undefined) {
            callback = function () {
                location.href = document.referrer;
            }
        }

        $(selector).parsley().on('form:submit', function (form) {

            let url = form.element.action;
            if (url === undefined || url === '' || url === null) {
                url = window.location.href;
            }
            let data = $(form.element).serializeArray();
            let editorList = document.querySelectorAll(".editor");

            if (editorList.length > 0) {
                $.each(editorList, function (i, v) {
                    let name = $(this).attr("id");
                    if (name) {
                        let content = '';
                        if (defaultEditor === '' || defaultEditor === 'ck') {
                            content = CKEDITOR.instances[name].getData();
                        } else {
                            content = UE.getEditor(name).getContent()
                        }

                        data.push({name: name, value: content})
                    }
                });
            }

            myAdmin.request.post(url, data, function (res) {
                myAdmin.message.success(res.msg, callback);
            });

            return false;
        });
    },
    editor: function () {

        let editorList = document.querySelectorAll(".editor");

        if (editorList.length > 0) {
            if (defaultEditor === '' || defaultEditor === 'ck') {
                $.each(editorList, function (i, v) {
                    CKEDITOR.replace(
                        $(this).attr("name"),
                        {
                            height: 400,
                            filebrowserImageUploadUrl: '/system/upload',
                            fileTools_requestHeaders: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                });
            } else {
                $.each(editorList, function (i, v) {
                    UE.getEditor($(this).attr("name"), {
                        initialFrameWidth: null,
                        initialFrameHeight: 400,
                        serverUrl: '/system/upload'
                    });
                });
            }
        }

    },
    upload: function () {

        $('input[cms-upload]').each(function (index, item) {
            let object = $(item);

            let exts = object.data("upload-exts");
            let maxFilesize = object.data("max-size");
            let number = object.data("upload-number");
            let name = object.data("name");

            if (myAdmin.uploadObjArray[name] === undefined) {

                object.dropzone({
                    url: '/system/upload',
                    //maxFiles: number ? null : 1,
                    maxFilesize: maxFilesize ? maxFilesize : 2,
                    acceptedFiles: exts ? exts : ".ico,.png,.jpg,.jpeg",
                    //dictMaxFilesExceeded: "最多可以上传" + number + "个文件",
                    init: function () {
                        this.on("success", function (file, data) {
                            data = eval('(' + data + ')');
                            $('input[name="' + name + '"]').val(data.url);
                            $('input[name="' + name + '"]')
                                .parents(".cms-upload").find(".upload-thumb")
                                .html('<img class="img-thumbnail mx-1" style="width: 100px;max-height: 100px" src="' + data.url + '" data-holder-rendered="true">');
                        });
                        this.on("error", function (file, data) {
                            myAdmin.message.error(data.msg ? data.msg : data);
                        });
                    }
                });

                let img = $('input[name="' + name + '"]').val();

                if (img) {

                    $('input[name="' + name + '"]')
                        .parents(".cms-upload").find(".upload-thumb")
                        .html('<img class="img-thumbnail mx-1" style="width: 100px;max-height: 100px" src="' + img + '" data-holder-rendered="true">');
                }

                object.click(function (e) {
                    e.preventDefault();
                });

                myAdmin.uploadObjArray[name] = 1;
            }

        });
    },
    table: function (init, cols, toolbar, page, params) {

        page = page ? page : 1;
        params = params !== undefined ? params : {};
        params = Object.assign(params, {page: page});

        page === 1 && $('.btn-toolbar .button-items').html(myAdmin.tableToolBar(init, toolbar));

        $('.search-bar').html(myAdmin.tableSearchBar(init, cols));

        $('#search-btn').click(function () {

            let searchParams = {};

            let type = $('#filterType').val();
            let val = $('#filterVal').val();

            searchParams[type] = val;

            myAdmin.table(init, cols, toolbar, page, {filter: JSON.stringify(searchParams)});
        });

        myAdmin.request.get(init.index_url, params, function (res) {

            let colsLength = cols.length;
            let theadHtml = '<thead><tr>';
            let tbody = '<tbody>';

            for (let i = 0; i < colsLength; i++) {
                if (cols[i].type === 'checkbox') {
                    theadHtml += '<th><input class="form-check-input" type="checkbox" id="checkAll"></th>';
                } else if (cols[i]['title'] !== undefined) {
                    theadHtml += '<th>' + cols[i].title + '</th>';
                }
            }

            theadHtml += '</tr></thead>';

            if (res.total > 0) {
                for (let i in res.data) {

                    tbody += '<tr>';
                    let data = res.data[i];

                    for (let cl = 0; cl < colsLength; cl++) {

                        if (cols[cl]['field'] !== undefined) {

                            let value = '';
                            let fields = cols[cl]['field'].split(".");

                            if (fields.length === 1) {
                                value = data[fields[0]] !== undefined ? data[fields[0]] : '';
                            } else {
                                value = data[fields[0]] ? data[fields[0]][fields[1]] : '';
                            }

                            if (cols[cl]['callback'] !== undefined) {
                                value = cols[cl]['callback'](value);
                            }

                            if (cols[cl]['selectList'] !== undefined) {
                                value = '<div class="form-check form-switch mb-3" dir="ltr">\n' +
                                    '   <input type="checkbox" class="form-check-input customSwitch" data-id="' + data['id'] + '" data-field="' + cols[cl]['field'] + '" ' + (cols[cl]['selectList'][value] ? 'checked=""' : '') + '>\n' +
                                    '</div>';
                            } else if (cols[cl].type === 'image') {

                                if (value) {
                                    value = '<img src="' + value + '" class="rounded avatar-sm">';
                                }
                            }

                            tbody += '<td>' + value + '</td>';

                        } else if (cols[cl].type === 'checkbox') {

                            let field = cols[cl].name ? cols[cl].name : 'id';
                            tbody += '<td><input class="form-check-input" type="checkbox" name="ids[]" value="' + (data[field] ? data[field] : '') + '"></td>';

                        } else if (cols[cl]['operate'] !== undefined) {
                            tbody += myAdmin.tableOperate(init, cols[cl]['operate'], data[cols[cl].name ? cols[cl].name : 'id']);
                        }
                    }

                    tbody += '</tr>';
                }
            }

            tbody += '</tbody>';

            $(init.table_elem).html(theadHtml + tbody);

            $(".table-responsive").responsiveTable({addDisplayAllBtn: false, addFocusBtn: false});

            $(init.table_elem).DataTable({
                "searching": false,
                "ordering": false,
                "lengthChange": false,
                "pageLength": res.per_page,
                "initUrl": init.index_url,
                "pageTotal": res.total,
                "currentPage": res.current_page,
                "lastPage": res.last_page,
                "destroy": true,
                "language": {
                    "info": "共" + res.total + "条记录，当前是第" + res.current_page + "页，共" + res.last_page + "页",
                    "infoEmpty": "共0条记录",
                    "infoFiltered": "",
                    "emptyTable": "没有找到数据记录",
                    "paginate": {
                        "first": "首页",
                        "last": "末页",
                        "next": "下一页",
                        "previous": "上一页"
                    },
                }
            });

            $('.page-item').on('click', function () {
                let page = $(this).text();
                switch (page) {
                    case "上一页":
                        page = res.current_page - 1;
                        break;
                    case "下一页":
                        page = res.current_page + 1;
                        break;
                }

                if (page > 0 && page <= res.last_page) {
                    myAdmin.table(init, cols, toolbar, page, params);
                }
            });

            $("#checkAll").click(function () {
                let element = $(init.table_elem).find("[name='ids[]']");
                if ($(this).is(":checked")) {
                    element.attr("checked", true);
                } else {
                    element.attr("checked", false);
                }
            });

            $('.customSwitch').change(function () {
                if (init.modify_url !== undefined) {
                    let data = {
                        id: $(this).data('id'),
                        field: $(this).data('field'),
                        value: $(this).is(':checked') ? 1 : 0,
                    };
                    myAdmin.request.post(init.modify_url, data)
                }
            });

            $('.btn[data-request]').click(function () {
                let title = $(this).data('title');
                let request = $(this).data('request');

                myAdmin.message.confirm(title, function () {
                    myAdmin.request.get(request, {}, function (res) {
                        myAdmin.message.success(res.msg, function () {
                            location.reload();
                        });
                    });
                })
            });

            myAdmin.deleteOperate(init);
        });
    },
    tableOperate: function (init, operate, primaryId) {

        let operateHtml = '<td>';

        for (let i = 0; i < operate.length; i++) {

            if (operate[i] === 'edit') {
                operateHtml += '<a href="' + myAdmin.concatQueryParam(init.edit_url, [{
                    key: "id",
                    val: primaryId
                }]) + '" class="mx-1 btn btn-sm btn-primary waves-effect waves-light">编辑</a>';
            } else if (operate[i] === 'delete') {
                operateHtml += '<a href="javascript:" data-id="' + primaryId + '" class="deleteOperate mx-1 btn btn-sm btn-danger waves-effect waves-light">删除</a>';
            } else if (typeof operate[i] === 'object') {
                let target = operate[i]['target'] ? operate[i]['target'] : '_self';
                operateHtml += '<a href="' + myAdmin.concatQueryParam(operate[i]['url'], [{
                    key: "id",
                    val: primaryId
                }]) + '" class="mx-1 btn btn-sm ' + operate[i]['class'] + ' waves-effect waves-light" target="' + target + '">' + operate[i]['text'] + '</a>';
            }
        }

        operateHtml += '</td>';

        return operateHtml;
    },
    tableToolBar: function (init, toolbar) {

        toolbar = toolbar ? toolbar : ['add', 'delete'];

        let toolbarHtml = '';

        for (let i = 0; i < toolbar.length; i++) {

            if (toolbar[i] === 'add') {
                toolbarHtml += '<a href="' + init.add_url + '" class="mx-1 btn btn-primary waves-effect waves-light">添加</a>';
            } else if (toolbar[i] === 'delete') {
                toolbarHtml += '<a href="javascript:" id="deleteToolBar" class="mx-1 btn btn-danger waves-effect waves-light">删除</a>';
            } else if (toolbar[i] === 'config') {
                toolbarHtml += '<a href="' + init.config_url + '" class="mx-1 btn btn-light waves-effect waves-light">配置</a>';
            } else if (typeof toolbar[i] === 'object') {
                toolbarHtml += '<a href="' + toolbar[i]['url'] + '" class="mx-1 btn ' + toolbar[i]['class'] + ' waves-effect waves-light">' + toolbar[i]['text'] + '</a>';
            }
        }

        toolbarHtml += '';

        return toolbarHtml;
    },
    deleteOperate: function (init) {

        $('#deleteToolBar').click(function () {
            if ($('input[name="ids[]"]:checked').length > 0) {
                myAdmin.message.confirm("是否确认删除这些记录？", function () {
                    let ids = [];
                    $('input[name="ids[]"]:checked').each(function () {
                        ids.push($(this).val());
                    });
                    myAdmin.request.post(init.delete_url, {id: ids}, function () {
                        myAdmin.message.success("删除成功", function () {
                            location.reload();
                        })
                    });
                });
            } else {
                myAdmin.message.error("请选择要删除的记录")
            }
        });

        $('.deleteOperate').click(function () {
            let id = $(this).data('id');
            myAdmin.message.confirm("是否确认删除该记录？", function () {
                myAdmin.request.post(init.delete_url, {id: id}, function () {
                    myAdmin.message.success("删除成功", function () {
                        location.reload();
                    })
                });
            });
        });
    },
    tableSearchBar: function (init, cols) {

        let html = '';
        let searchFields = [];

        for (let i = 0; i < cols.length; i++) {
            if (cols[i]['search'] === true) {
                searchFields.push(cols[i]);
            }
        }

        if (searchFields.length > 0) {

            html += '<select class="form-select" id="filterType">\n';
            for (let i = 0; i < searchFields.length; i++) {
                html += '<option value="' + searchFields[i].field + '">' + searchFields[i].title + '</option>';
            }
            html += '</select>';

            html += '<input type="text" class="form-control" placeholder="请输入搜索内容" id="filterVal" value="">';
            html += '<a class="btn btn-primary btn-sm" id="search-btn">搜索</a>';
        }

        return html;
    },
    message: {
        common: function (message, icon, callback, title) {
            let alert = Swal.fire({
                title: title ? title : '系统提示',
                text: message ? message : '操作失败',
                icon: icon ? icon : "success",
                confirmButtonColor: "#525ce5",
            });

            if (callback !== undefined) {
                alert.then(callback);
            }
        },
        success: function (message, callback, title) {
            myAdmin.message.common(message, "success", callback, title);
        },
        error: function (message, callback, title) {
            myAdmin.message.common(message, "warning", callback, title);
        },
        confirm: function (message, callback) {
            let alert = Swal.fire({
                title: '系统提示',
                text: message,
                icon: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#1cbb8c",
                cancelButtonColor: "#f14e4e",
                confirmButtonText: "确定",
                cancelButtonText: "取消",
            });

            if (callback !== undefined) {
                alert.then(function (t) {
                    t.value && callback();
                });
            }
        }
    },
    request: {
        post: function (url, data, callback) {
            myAdmin.request.ajax(url, "POST", data, callback);
        },
        get: function (url, data, callback) {
            myAdmin.request.ajax(url, "GET", data, callback);
        },
        ajax: function (url, method, data, callback) {
            $.ajax({
                url: url,
                type: method,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                dataType: "json",
                data: data,
                timeout: 60000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {
                    if (callback !== undefined) {
                        callback(res);
                    }
                },
                error: function (res) {
                    myAdmin.message.error(res.responseJSON.msg);
                }
            });
        }
    },
    getQueryParam: function (key, defaultVal) {
        let url = location.search; //获取url中"?"符后的字串
        let theRequest = new Object();
        if (url.indexOf("?") !== -1) {
            let str = url.substr(1);
            let strArray = str.split("&");
            for (let i = 0; i < strArray.length; i++) {
                theRequest[strArray[i].split("=")[0]] = unescape(strArray[i].split("=")[1]);
            }
        }
        return theRequest[key]
            ? theRequest[key]
            : (defaultVal !== undefined ? defaultVal : '');
    },
    concatQueryParam: function (url, array) {

        let delimiter = '?';
        if (url.indexOf("?") !== -1) {
            delimiter = '&';
        }

        url = url + delimiter;
        for (let i = 0; i < array.length; i++) {
            url += array[i]['key'] + '=' + array[i]['val'];
        }

        return url;
    }
};

