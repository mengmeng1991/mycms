@extends("layouts.common")
@section('page-content-wrapper')

    <div class="table-rep-plugin">
        <div class="btn-toolbar">
            <div class="button-items btn-group"></div>
            <div class="search-bar btn-group pull-right"></div>
        </div>
        <div class="table-responsive mb-0" data-pattern="priority-columns">
            <table id="currentTable" class="table table-striped">
            </table>
        </div>
    </div>
@endsection
@section('extend-javascript')
    <script>
        myAdmin.table({
            table_elem: '#currentTable',
            index_url: '/user/admin',
            add_url: '/user/admin/create',
            edit_url: '/user/admin/edit',
            delete_url: '/user/admin/destroy',
            modify_url: '/user/admin/modify',
            password_url: '/user/admin/password',
            account_url: '/user/admin/account',
            rank_url: '/user/admin/rank',
            address_url: '/user/admin/address',
        }, [
            {type: "checkbox"},
            {field: 'id', width: 80, title: 'ID'},
            {field: 'name', minWidth: 80, title: '用户名', search: true},
            {field: 'mobile', minWidth: 80, title: '手机号', search: true},
            {
                field: 'user_rank.name', minWidth: 80, title: '会员等级'
            },
            {field: 'openid', minWidth: 80, title: 'openid'},
            {field: 'balance', minWidth: 80, title: '余额'},
            {field: 'point', minWidth: 80, title: '积分'},
            {
                field: 'status',
                title: '状态',
                width: 85,
                search: 'select',
                selectList: {0: '禁用', 1: '启用'},
            },
            {field: 'created_at', minWidth: 120, title: '创建时间'},
            {field: 'updated_at', minWidth: 120, title: '更新时间'},
            {
                width: 250,
                title: '操作',
                operate: [
                    'edit',
                    {
                        text: '重置密码',
                        url: '/user/admin/password',
                        class: '',
                    },
                    {
                        text: '资金变动',
                        url: '/user/admin/account',
                        class: '',
                    },
                    'delete'
                ]
            }
        ]);
    </script>
@endsection
