@extends("layouts.common")
@section('page-content-wrapper')
    <script>
        var mps = {!! json_encode(config('mp.platform')) !!};
    </script>
    <div class="table-rep-plugin">

        <div class="btn-toolbar">
            <div class="button-items btn-group"></div>
            <div class="search-bar btn-group pull-right"></div>
        </div>
        <div class="table-responsive mb-0" data-pattern="priority-columns">
            <table id="currentTable" class="table table-striped">
            </table>
        </div>
    </div>
@endsection
@section('extend-javascript')
    <script>
        myAdmin.table({
            table_elem: '#currentTable',
            index_url: '/mp/admin/mp_account',
            add_url: '/mp/admin/mp_account/create',
            edit_url: '/mp/admin/mp_account/edit',
            delete_url: '/mp/admin/mp_account/destroy',
        }, [
            {type: "checkbox"},
            {field: 'name', minWidth: 80, title: '名称'},
            {
                field: 'type', minWidth: 80, title: '平台类型', templet: function (item) {
                    return mps[item.type];
                }
            },
            {field: 'app_id', minWidth: 80, title: 'AppId'},
            {field: 'app_key', minWidth: 80, title: 'AppKey'},

            {field: 'created_at', minWidth: 120, title: '创建时间'},
            {field: 'updated_at', minWidth: 120, title: '更新时间'},
            {
                width: 250,
                title: '操作',
                operate: [
                    'edit',
                    'delete'
                ]
            }
        ]);
    </script>
@endsection
