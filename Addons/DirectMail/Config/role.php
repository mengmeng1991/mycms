<?php

return [
    'addon.directmail' => '阿里云邮件推送',
    'addon/directmail' => '邮件发信管理',
    'addon/directmail/edit' => '编辑邮件配置',
    'addon/directmail/create' => '添加邮件配置',
    'addon/directmail/destroy' => '删除邮件配置',
    'addon/directmail/logs' => '邮件发送记录',
];
