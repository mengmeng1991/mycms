<?php

return [
    'addon.alisms' => '阿里云短信',
    'addon/alisms' => '短信模板管理',
    'addon/alisms/edit' => '编辑短信模板',
    'addon/alisms/create' => '添加短信模板',
    'addon/alisms/destroy' => '删除短信模板',
    'addon/alisms/logs' => '短信发送记录',
];
