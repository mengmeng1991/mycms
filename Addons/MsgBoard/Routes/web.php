<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix' => 'addon/msg_board',
        'namespace' => 'Addons\MsgBoard\Controllers'
    ], function () {

    Route::group(['middleware' => 'admin.auth'], function () {
        Route::get('/', 'MsgBoardAdminController@index')->name('addon.msg_board.index');
        Route::get('/edit', 'MsgBoardAdminController@edit')->name('addon.msg_board.edit');
        Route::post('/edit', 'MsgBoardAdminController@update');
        Route::post('/destroy', 'MsgBoardAdminController@destroy');
    });

    Route::post('submit', 'MsgBoardWebController@submit')->middleware('throttle:3,1')->name('addon.msgBoard.submit');

});
