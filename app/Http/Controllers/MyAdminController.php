<?php


namespace App\Http\Controllers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class MyAdminController extends MyController
{

    public $model = '';

    public $request = '';

    public $view = 'admin.';

    public $with = [];

    public $editWith = [];

    public function __call($method, $parameters)
    {
        if (method_exists($this, "{$method}Action")) {
            return $this->{"{$method}Action"}();
        }

        parent::__call($method, $parameters);
    }

    /**
     * 获取关联模型
     */
    public function getModel(): Model
    {
        return (new $this->model);
    }

    /**
     * 获取管理请求对象
     */
    public function getRequest(): FormRequest
    {
        return app($this->request);
    }

    /**
     * 首页
     */
    public function indexAction()
    {
        if (request()->ajax() && request()->wantsJson()) {

            $data = $this->getModel()::with($this->with)
                ->where($this->getIndexWhere())
                ->orderBy('id', 'desc')
                ->paginate($this->request('limit', 'intval'))
                ->toArray();

            return $this->jsonSuc($data);
        }

        return $this->view($this->view . 'index');
    }


    /**
     * @return array
     */
    public function getIndexWhere(): array
    {
        return [];
    }


    /**
     * 创建页
     */
    public function createAction()
    {
        return $this->view($this->view . 'create');
    }

    /**
     * 创建记录
     */
    public function storeAction(): JsonResponse
    {
        $data = $this->getRequest()->validated();

        $model = $this->getModel();
        $result = $model->store($data);

        if (method_exists($this, 'afterStore')) {
            $result = $this->afterStore($model->id);
        }

        return $this->result($result);
    }

    /**
     * 编辑页
     */
    public function editAction()
    {
        $data = $this->getModel()::with($this->editWith)
            ->find($this->request('id', 'intval'));

        return $this->view($this->view . 'edit', compact('data'));
    }

    /**
     * 更新记录
     */
    public function updateAction(): JsonResponse
    {

        if ($id = $this->request('id', 'intval')) {

            $data = $this->getRequest()->validated();
            $data['id'] = $id;

            $model = $this->getModel();
            $result = $model->up($data);

            if (method_exists($this, 'afterUpdate')) {
                $result = $this->afterUpdate($id);
            }

            return $this->result($result);
        }

        return $this->result(false);
    }

    /**
     * 删除记录
     */
    public function destroyAction(): JsonResponse
    {
        $id = $this->request('id', 'intval');
        $result = $this->getModel()::destroy($id);

        if (method_exists($this, 'afterDestroy')) {
            $result = $this->afterDestroy($id);
        }

        return $this->result($result);
    }


    /**
     * 修改属性
     * @return JsonResponse
     */
    public function modifyAction(): JsonResponse
    {
        $result = false;

        if ($id = $this->request('id', 'intval')) {

            $object = $this->getModel()::find($id);
            $object->{$this->request('field')} = $this->request('value');
            $result = $object->save();
        }

        return $this->result($result);
    }
}
